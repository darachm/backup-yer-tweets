# The below copy pasted from https://gist.github.com/joelnitta/48a8c3b80fafc0e06dde9c39e5841915 !!!!

library(rtweet)
library(tidyverse)

# Initial authorization setup, only need to do once
# auth_setup_default() #nolint

# Authorize
auth_as("default")

# Set user name
user_name <- "PUT USER NAME HERE"

# Download all tweets from user (as many as possible anyways)
# The docs say it should work for up to 3200 tweets
# https://docs.ropensci.org/rtweet/reference/get_timeline.html
my_tweets <- get_timeline(
  user = user_name,
  n = Inf,
  retryonratelimit = TRUE)

# And save them forever!
saveRDS(my_tweets, "my_tweets.RDS")

# Extract URLs of media (images etc)
# We can map back to the tweet the image came from by `tweet_id`, which
# matches `id` of my_tweets
media_url <-
  my_tweets %>%
  rename(tweet_id = id) %>%
  mutate(media = map(entities, "media")) %>%
  select(tweet_id, media) %>%
  unnest(media) %>%
  filter(!is.na(id)) %>%
  mutate(
    filename = str_match(media_url, "\\/([^\\/]*)$") |>
      magrittr::extract(, 2)
  )

# Download media to "media" folder
walk2(
  media_url$media_url,
  media_url$filename,
  ~download.file(.x, glue::glue("media/{.y}")))
